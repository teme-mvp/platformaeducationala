﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tema_3.Helpers;
using Tema_3.ViewModels;
using Tema_3.Views;

namespace Tema_3.Models.Actions
{
    class AbsenceActions : BaseVM
    {
        ScoalaEntities context = new ScoalaEntities();

        private AbsenceVM absenceContext;
        public AbsenceActions(AbsenceVM absenceContext)
        {
            this.absenceContext = absenceContext;
        }

        public void AddMethod(object obj)
        {
            AbsenceVM absenceVM = obj as AbsenceVM;
            if (absenceVM != null)
            {
                if (String.IsNullOrEmpty(absenceVM.TeacherName))
                {
                    absenceContext.Message = "Numele profesorului trebuie precizat";
                }
                else
                {
                    List<Profesor> profi = context.Profesors.ToList();
                    foreach (Profesor prof in profi)
                    {
                        if (prof.nume.Equals(absenceVM.TeacherName))
                        {
                            List<Profesor_Clasa> repartizari = context.Profesor_Clasa.ToList();
                            foreach (Profesor_Clasa rep in repartizari)
                            {
                                if (rep.nume_profesor.Equals(prof.nume))
                                {
                                    if (rep.nume_materie.Equals(absenceVM.DisciplineName))
                                    {
                                        context.InsertAbsenta(context.Absentas.OrderByDescending(p => p.id_absenta).FirstOrDefault().id_absenta + 1, absenceVM.AbsenceDate, absenceVM.Nemotivabila, context.Elevs.OrderByDescending(p => p.cnp_elev).FirstOrDefault().cnp_elev, absenceVM.DisciplineName, absenceVM.TeacherName, absenceVM.StudentName, absenceVM.ClassName);
                                        context.SaveChanges();
                                        absenceContext.AbsencesList = AllAbsences(absenceVM.TeacherName);
                                        absenceContext.Message = "";
                                        return;
                                    }
                                }
                            }
                            MessageBox.Show("Profesorul introdus nu preda materia introdusa!");
                            return;
                        }
                    }
                    MessageBox.Show("Profesorul introdus nu se afla in baza de date!");
                    return;
                }
            }
        }

        public void MotivareMethod(object obj)
        {
            AbsenceVM absenceVM = obj as AbsenceVM;

            if (absenceVM.TeacherName == null || absenceVM.TeacherName == "")
            {
                absenceContext.Message = "Selectati o absenta!";
            }
            else
            {
                context.MotivareAbsenta(absenceVM.StudentName, absenceVM.ClassName, absenceVM.DisciplineName, absenceVM.AbsenceDate);
                context.SaveChanges();
                absenceContext.AbsencesList = AllAbsences(absenceVM.TeacherName);
                absenceContext.Message = "";
            }
        }

        public void BackMethod(object obj)
        {
            EditAbsenceWindow mainWindow = (Application.Current.MainWindow as EditAbsenceWindow);
            Application.Current.MainWindow = new TeacherWindow();
            Application.Current.MainWindow.Show();
            mainWindow.Close();
        }

        public void RefreshMethod(object obj)
        {
            if (String.IsNullOrEmpty(obj.ToString()))
            {
                absenceContext.Message = "Trebuie sa precizati numele dumneavoastra!";
            }
            else
            {
                absenceContext.AbsencesList = AllAbsences(obj.ToString());
                absenceContext.Message = "";
            }
        }

        public ObservableCollection<AbsenceVM> AllAbsences(string profesor)
        {
            List<Absenta> absences = context.Absentas.ToList();
            ObservableCollection<AbsenceVM> result = new ObservableCollection<AbsenceVM>();
            foreach (Absenta absenta in absences)
            {
                if (absenta.nume_profesor.Equals(profesor))
                    result.Add(new AbsenceVM()
                    {
                        AbsenceId = absenta.id_absenta,
                        AbsenceDate = absenta.data_absenta,
                        Nemotivabila = absenta.nemotivabila,
                        Motivata = absenta.motivata,
                        DisciplineId = absenta.id_materie,
                        StudentCnp = absenta.cnp_elev,
                        DisciplineName = absenta.nume_materie,
                        TeacherName = absenta.nume_profesor,
                        StudentName = absenta.nume_elev,
                        ClassName = absenta.nume_clasa
                    });
            }
            return result;
        }
    }
}
