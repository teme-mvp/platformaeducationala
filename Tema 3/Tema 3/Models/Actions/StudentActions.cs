﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tema_3.Helpers;
using Tema_3.ViewModels;
using Tema_3.Views;

namespace Tema_3.Models.Actions
{
    class StudentActions : BaseVM
    {
        ScoalaEntities context = new ScoalaEntities();

        private StudentVM studentContext;
        public StudentActions(StudentVM studentContext)
        {
            this.studentContext = studentContext;
        }

        public void AddMethod(object obj)
        {
            StudentVM studentVM = obj as StudentVM;
            if (studentVM != null)
            {
                if (String.IsNullOrEmpty(studentVM.Name))
                {
                    studentContext.Message = "Numele elevului trebuie precizat";
                }
                else
                {
                    List<Elev> elevi = context.Elevs.ToList();
                    foreach(Elev elev in elevi)
                    {
                        if(elev.cnp_elev == studentVM.StudentCnp)
                        {
                            MessageBox.Show("Elevul cu acest cnp se afla deja in baza de date!");
                            return;
                        }
                    }
                    context.Elevs.Add(new Elev() { cnp_elev = studentVM.StudentCnp, nume = studentVM.Name, nr_tel = studentVM.PhoneNr, id_clasa = studentVM.ClassId, id_cont = studentVM.AccountId, nume_clasa = studentVM.ClassName });
                    context.SaveChanges();
                    studentContext.StudentsList = AllStudents();
                    studentContext.Message = "";
                }
            }
        }

        public void UpdateMethod(object obj)
        {
            StudentVM studentVM = obj as StudentVM;

            if (String.IsNullOrEmpty(studentVM.StudentCnp))
            {
                studentContext.Message = "CNP-ul elevului trebuie precizat";
            }
            else
            {
                context.UpdateStudentByCnp(studentVM.StudentCnp, studentVM.Name, studentVM.PhoneNr, studentVM.ClassName);
                context.SaveChanges();
                studentContext.StudentsList = AllStudents();
                studentContext.Message = "";
            }
        }

        public void DeleteMethod(object obj)
        {
            StudentVM studentVM = obj as StudentVM;

            if (studentVM.Name == null || studentVM.Name == "")
            {
                studentContext.Message = "Selectati un elev!";
            }
            else
            {
                context.DeleteElev(studentVM.Name);
                context.SaveChanges();
                studentContext.StudentsList = AllStudents();
                studentContext.Message = "";
            }
        }

        public void BackMethod(object obj)
        {
            EditStudentWindow mainWindow = (Application.Current.MainWindow as EditStudentWindow);
            Application.Current.MainWindow = new AdminWindow();
            Application.Current.MainWindow.Show();
            mainWindow.Close();
        }

        public ObservableCollection<StudentVM> AllStudents()
        {
            List<Elev> students = context.Elevs.ToList();
            ObservableCollection<StudentVM> result = new ObservableCollection<StudentVM>();
            foreach (Elev elev in students)
            {
                result.Add(new StudentVM()
                {
                    StudentCnp = elev.cnp_elev,
                    Name = elev.nume,
                    PhoneNr = elev.nr_tel,
                    ClassId = (int)elev.id_clasa,
                    AccountId = elev.id_cont,
                    ClassName = elev.nume_clasa
                });
            }
            return result;
        }
    }
}
