﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tema_3.Helpers;
using Tema_3.ViewModels;
using Tema_3.Views;

namespace Tema_3.Models.Actions
{
    class AllocationActions : BaseVM
    {
        ScoalaEntities context = new ScoalaEntities();

        private AllocationVM allocationContext;
        public AllocationActions(AllocationVM allocationContext)
        {
            this.allocationContext = allocationContext;
        }

        public void AddMethod(object obj)
        {
            AllocationVM allocationVM = obj as AllocationVM;
            if (allocationVM != null)
            {
                if (String.IsNullOrEmpty(allocationVM.TeacherCnp))
                {
                    allocationContext.Message = "Cnp-ul profesorului trebuie precizat";
                }
                else
                {
                    List<Profesor> profi = context.Profesors.ToList();
                    foreach (Profesor prof in profi)
                    {
                        if (prof.nume == allocationVM.TeacherName)
                        {
                            context.Profesor_Clasa.Add(new Profesor_Clasa() { id_profesor_clasa = context.Profesor_Clasa.OrderByDescending(p => p.id_profesor_clasa).FirstOrDefault().id_profesor_clasa + 1, cnp_profesor = allocationVM.TeacherCnp, id_clasa = allocationVM.ClassId, id_materie = allocationVM.DisciplineId, nume_profesor = allocationVM.TeacherName, nume_clasa = allocationVM.ClassName, nume_materie = allocationVM.DisciplineName });
                            context.SaveChanges();
                            allocationContext.AllocationsList = AllAllocations();
                            allocationContext.Message = "";
                            return;
                        }
                    }
                    MessageBox.Show("Profesorul nu se afla in baza de date!");
                    return;
                }
            }
        }

        public void UpdateMethod(object obj)
        {
            AllocationVM allocationVM = obj as AllocationVM;

            if (String.IsNullOrEmpty(allocationVM.TeacherCnp))
            {
                allocationContext.Message = "Cnp-ul profesorului trebuie precizat";
            }
            else
            {
                context.UpdateAllocation(allocationVM.TeacherCnp, allocationVM.ClassName, allocationVM.DisciplineName);
                context.SaveChanges();
                allocationContext.AllocationsList = AllAllocations();
                allocationContext.Message = "";
            }
        }

        public void DeleteMethod(object obj)
        {
            AllocationVM allocationVM = obj as AllocationVM;

            if (allocationVM.TeacherCnp == null || allocationVM.TeacherCnp == "")
            {
                allocationContext.Message = "Selectati o repartizare!";
            }
            else
            {

                context.DeleteAllocation(allocationVM.TeacherClassId);
                context.SaveChanges();
                allocationContext.AllocationsList = AllAllocations();
                allocationContext.Message = "";
            }
        }

        public void BackMethod(object obj)
        {
            EditAllocationWindow mainWindow = (Application.Current.MainWindow as EditAllocationWindow);
            Application.Current.MainWindow = new AdminWindow();
            Application.Current.MainWindow.Show();
            mainWindow.Close();
        }

        public ObservableCollection<AllocationVM> AllAllocations()
        {
            List<Profesor_Clasa> allocations = context.Profesor_Clasa.ToList();
            ObservableCollection<AllocationVM> result = new ObservableCollection<AllocationVM>();
            foreach (Profesor_Clasa allocation in allocations)
            {
                result.Add(new AllocationVM()
                {
                    TeacherClassId = allocation.id_profesor_clasa,
                    TeacherCnp = allocation.cnp_profesor,
                    DisciplineId = allocation.id_materie,
                    ClassId = allocation.id_clasa,
                    TeacherName = allocation.nume_profesor,
                    ClassName = allocation.nume_clasa,
                    DisciplineName = allocation.nume_materie
                });
            }
            return result;
        }
    }
}
