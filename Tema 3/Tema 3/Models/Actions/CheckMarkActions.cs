﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tema_3.Helpers;
using Tema_3.ViewModels;
using Tema_3.Views;

namespace Tema_3.Models.Actions
{
    class CheckMarkActions : BaseVM
    {
        ScoalaEntities context = new ScoalaEntities();

        private CheckMarksVM markContext;
        public CheckMarkActions(CheckMarksVM markContext)
        {
            this.markContext = markContext;
        }

        public void SeeByDMethod(object obj)
        {
            CheckMarksVM markVM = obj as CheckMarksVM;
            if (markVM != null)
            {
                if (String.IsNullOrEmpty(markVM.StudentName))
                {
                    markContext.Message = "Numele tau trebuie precizat!";
                }
                else
                {
                    markContext.CheckMarksList = SomeMarks(markVM.StudentName, markVM.DisciplineName);
                    markContext.Message = "";
                }
            }
        }

        public void SeeAllMethod(object obj)
        {
            CheckMarksVM markVM = obj as CheckMarksVM;
            if (markVM != null)
            {
                if (String.IsNullOrEmpty(markVM.StudentName))
                {
                    markContext.Message = "Numele tau trebuie precizat!";
                }
                else
                {
                    markContext.CheckMarksList = AllMarks(markVM.StudentName);
                    markContext.Message = "";
                }
            }
        }

        public void BackMethod(object obj)
        {
            CheckMarksWindow mainWindow = (Application.Current.MainWindow as CheckMarksWindow);
            Application.Current.MainWindow = new StudentWindow();
            Application.Current.MainWindow.Show();
            mainWindow.Close();
        }

        public ObservableCollection<CheckMarksVM> AllMarks(string elev)
        {
            List<Nota> marks = context.Notas.ToList();
            ObservableCollection<CheckMarksVM> result = new ObservableCollection<CheckMarksVM>();
            foreach (Nota nota in marks)
            {
                if (nota.nume_elev.Equals(elev))
                    result.Add(new CheckMarksVM()
                    {
                        MarkId = nota.id_nota,
                        ActualMark = nota.nota1,
                        MarkDate = nota.data_nota,
                        DisciplineId = nota.id_materie,
                        StudentCnp = nota.cnp_elev,
                        DisciplineName = nota.nume_materie,
                        StudentName = nota.nume_elev,
                        TeacherName = nota.nume_profesor
                    });
            }
            return result;
        }

        public ObservableCollection<CheckMarksVM> SomeMarks(string elev, string materie)
        {
            List<Nota> marks = context.Notas.ToList();
            ObservableCollection<CheckMarksVM> result = new ObservableCollection<CheckMarksVM>();
            foreach (Nota nota in marks)
            {
                if (nota.nume_elev.Equals(elev) && nota.nume_materie.Equals(materie))
                    result.Add(new CheckMarksVM()
                    {
                        MarkId = nota.id_nota,
                        ActualMark = nota.nota1,
                        MarkDate = nota.data_nota,
                        DisciplineId = nota.id_materie,
                        StudentCnp = nota.cnp_elev,
                        DisciplineName = nota.nume_materie,
                        StudentName = nota.nume_elev,
                        TeacherName = nota.nume_profesor
                    });
            }
            return result;
        }
    }
}
