﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tema_3.Helpers;
using Tema_3.ViewModels;
using Tema_3.Views;

namespace Tema_3.Models.Actions
{
    class TeacherActions : BaseVM
    {
        ScoalaEntities context = new ScoalaEntities();

        private TeacherVM teacherContext;
        public TeacherActions(TeacherVM teacherContext)
        {
            this.teacherContext = teacherContext;
        }

        public void AddMethod(object obj)
        {
            TeacherVM teacherVM = obj as TeacherVM;
            if (teacherVM != null)
            {
                if (String.IsNullOrEmpty(teacherVM.Name))
                {
                    teacherContext.Message = "Numele profesorului trebuie precizat";
                }
                else
                {
                    List<Profesor> profi = context.Profesors.ToList();
                    foreach (Profesor prof in profi)
                    {
                        if (prof.cnp_profesor == teacherVM.TeacherCnp)
                        {
                            MessageBox.Show("Profesorul cu acest cnp se afla deja in baza de date!");
                            return;
                        }
                    }
                    context.Profesors.Add(new Profesor() { cnp_profesor = teacherVM.TeacherCnp, nume = teacherVM.Name, nr_tel = teacherVM.PhoneNr, rol = teacherVM.Role, id_clasa_dirig = teacherVM.ClassDirId, id_cont = teacherVM.AccountId, nume_clasa_dirig = teacherVM.ClassDirigName });
                    context.SaveChanges();
                    teacherContext.TeachersList = AllTeachers();
                    teacherContext.Message = "";
                }
            }
        }

        public void UpdateMethod(object obj)
        {
            TeacherVM teacherVM = obj as TeacherVM;

            if (String.IsNullOrEmpty(teacherVM.TeacherCnp))
            {
                teacherContext.Message = "CNP-ul profesorului trebuie precizat";
            }
            else
            {
                context.UpdateTeacherByCnp(teacherVM.TeacherCnp, teacherVM.Name, teacherVM.PhoneNr, teacherVM.Role, teacherVM.ClassDirigName);
                context.SaveChanges();
                teacherContext.TeachersList = AllTeachers();
                teacherContext.Message = "";
            }
        }

        public void DeleteMethod(object obj)
        {
            TeacherVM teacherVM = obj as TeacherVM;

            if (teacherVM.Name == null || teacherVM.Name == "")
            {
                teacherContext.Message = "Selectati un profesor!";
            }
            else
            {
                context.DeleteProfesor(teacherVM.Name);
                context.SaveChanges();
                teacherContext.TeachersList = AllTeachers();
                teacherContext.Message = "";
            }
        }

        public void BackMethod(object obj)
        {
            EditTeacherWindow mainWindow = (Application.Current.MainWindow as EditTeacherWindow);
            Application.Current.MainWindow = new AdminWindow();
            Application.Current.MainWindow.Show();
            mainWindow.Close();
        }

        public ObservableCollection<TeacherVM> AllTeachers()
        {
            List<Profesor> teachers = context.Profesors.ToList();
            ObservableCollection<TeacherVM> result = new ObservableCollection<TeacherVM>();
            foreach (Profesor prof in teachers)
            {
                result.Add(new TeacherVM()
                {
                    TeacherCnp = prof.cnp_profesor,
                    Name = prof.nume,
                    PhoneNr = prof.nr_tel,
                    Role = prof.rol,
                    ClassDirId = (int)prof.id_clasa_dirig,
                    AccountId = prof.id_cont,
                    ClassDirigName = prof.nume_clasa_dirig
                });
            }
            return result;
        }
    }
}
