﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tema_3.Helpers;
using Tema_3.ViewModels;
using Tema_3.Views;

namespace Tema_3.Models.Actions
{
    class DisciplineActions : BaseVM
    {
        ScoalaEntities context = new ScoalaEntities();

        private DisciplineVM disciplineContext;
        public DisciplineActions(DisciplineVM disciplineContext)
        {
            this.disciplineContext = disciplineContext;
        }

        public void AddMethod(object obj)
        {
            DisciplineVM disciplineVM = obj as DisciplineVM;
            if (disciplineVM != null)
            {
                if (String.IsNullOrEmpty(disciplineVM.Name))
                {
                    disciplineContext.Message = "Numele materiei trebuie precizat!";
                }
                else
                {

                    context.Materies.Add(new Materie() { id_materie = context.Materies.OrderByDescending(p => p.id_materie).FirstOrDefault().id_materie + 1, denumire = disciplineVM.Name, needs_teza = disciplineVM.NeedsT });
                    context.SaveChanges();
                    disciplineContext.DisciplinesList = AllDisciplines();
                    disciplineContext.Message = "";
                }
            }
        }

        public void UpdateMethod(object obj)
        {
            DisciplineVM disciplineVM = obj as DisciplineVM;

            if (String.IsNullOrEmpty(disciplineVM.Name))
            {
                disciplineContext.Message = "Numele materiei trebuie precizat";
            }
            else
            {
                context.UpdateMaterie(disciplineVM.Name, disciplineVM.NeedsT);
                context.SaveChanges();
                disciplineContext.DisciplinesList = AllDisciplines();
                disciplineContext.Message = "";
            }
        }

        public void DeleteMethod(object obj)
        {
            DisciplineVM disciplineVM = obj as DisciplineVM;

            if (disciplineVM == null)
            {
                disciplineContext.Message = "Selectati o materie!";
            }
            else
            {
                context.DeleteMaterie(disciplineVM.Name);
                context.SaveChanges();
                disciplineContext.DisciplinesList = AllDisciplines();
                disciplineContext.Message = "";
            }
        }

        public void BackMethod(object obj)
        {
            EditDisciplineWindow mainWindow = (Application.Current.MainWindow as EditDisciplineWindow);
            Application.Current.MainWindow = new AdminWindow();
            Application.Current.MainWindow.Show();
            mainWindow.Close();
        }

        public ObservableCollection<DisciplineVM> AllDisciplines()
        {
            List<Materie> disciplines = context.Materies.ToList();
            ObservableCollection<DisciplineVM> result = new ObservableCollection<DisciplineVM>();
            foreach (Materie discipline in disciplines)
            {
                result.Add(new DisciplineVM()
                {
                    DisciplineId = discipline.id_materie,
                    Name = discipline.denumire,
                    NeedsT = discipline.needs_teza
                });
            }
            return result;
        }
    }
}
