﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tema_3.Helpers;
using Tema_3.ViewModels;
using Tema_3.Views;

namespace Tema_3.Models.Actions
{
    class CheckAbsenceActions : BaseVM
    {
        ScoalaEntities context = new ScoalaEntities();

        private CheckAbsenceVM absenceContext;
        public CheckAbsenceActions(CheckAbsenceVM absenceContext)
        {
            this.absenceContext = absenceContext;
        }

        public void SeeByDMethod(object obj)
        {
            CheckAbsenceVM absenceVM = obj as CheckAbsenceVM;
            if (absenceVM != null)
            {
                if (String.IsNullOrEmpty(absenceVM.StudentName))
                {
                    absenceContext.Message = "Numele tau trebuie precizat!";
                }
                else
                {
                    absenceContext.CheckAbsencesList = SomeAbsences(absenceVM.StudentName, absenceVM.DisciplineName);
                    absenceContext.Message = "";
                }
            }
        }

        public void SeeAllMethod(object obj)
        {
            CheckAbsenceVM absenceVM = obj as CheckAbsenceVM;
            if (absenceVM != null)
            {
                if (String.IsNullOrEmpty(absenceVM.StudentName))
                {
                    absenceContext.Message = "Numele tau trebuie precizat!";
                }
                else
                {
                    absenceContext.CheckAbsencesList = AllAbsences(absenceVM.StudentName);
                    absenceContext.Message = "";
                }
            }
        }

        public void BackMethod(object obj)
        {
            CheckAbsencesWindow mainWindow = (Application.Current.MainWindow as CheckAbsencesWindow);
            Application.Current.MainWindow = new StudentWindow();
            Application.Current.MainWindow.Show();
            mainWindow.Close();
        }

        public ObservableCollection<CheckAbsenceVM> AllAbsences(string elev)
        {
            List<Absenta> absences = context.Absentas.ToList();
            ObservableCollection<CheckAbsenceVM> result = new ObservableCollection<CheckAbsenceVM>();
            foreach (Absenta absenta in absences)
            {
                if (absenta.nume_elev.Equals(elev))
                    result.Add(new CheckAbsenceVM()
                    {
                        AbsenceId = absenta.id_absenta,
                        AbsenceDate = absenta.data_absenta,
                        Nemotivabila = absenta.nemotivabila,
                        Motivata = absenta.motivata,
                        DisciplineId = absenta.id_materie,
                        StudentCnp = absenta.cnp_elev,
                        DisciplineName = absenta.nume_materie,
                        TeacherName = absenta.nume_profesor,
                        StudentName = absenta.nume_elev,
                        ClassName = absenta.nume_clasa
                    });
            }
            return result;
        }

        public ObservableCollection<CheckAbsenceVM> SomeAbsences(string elev, string materie)
        {
            List<Absenta> absences = context.Absentas.ToList();
            ObservableCollection<CheckAbsenceVM> result = new ObservableCollection<CheckAbsenceVM>();
            foreach (Absenta absenta in absences)
            {
                if (absenta.nume_elev.Equals(elev) && absenta.nume_materie.Equals(materie))
                    result.Add(new CheckAbsenceVM()
                    {
                        AbsenceId = absenta.id_absenta,
                        AbsenceDate = absenta.data_absenta,
                        Nemotivabila = absenta.nemotivabila,
                        Motivata = absenta.motivata,
                        DisciplineId = absenta.id_materie,
                        StudentCnp = absenta.cnp_elev,
                        DisciplineName = absenta.nume_materie,
                        TeacherName = absenta.nume_profesor,
                        StudentName = absenta.nume_elev,
                        ClassName = absenta.nume_clasa
                    });
            }
            return result;
        }
    }
}
