﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tema_3.Helpers;
using Tema_3.ViewModels;
using Tema_3.Views;

namespace Tema_3.Models.Actions
{
    class ClassAbsenceActions : BaseVM
    {
        ScoalaEntities context = new ScoalaEntities();

        private ClassAbsencesVM absenceContext;
        public ClassAbsenceActions(ClassAbsencesVM absenceContext)
        {
            this.absenceContext = absenceContext;
        }

        public void MotivareMethod(object obj)
        {
            ClassAbsencesVM absenceVM = obj as ClassAbsencesVM;

            if (absenceVM.TeacherName == null || absenceVM.TeacherName == "")
            {
                absenceContext.Message = "Selectati o absenta!";
            }
            else
            {
                context.MotivareAbsenta(absenceVM.StudentName, absenceVM.ClassName, absenceVM.DisciplineName, absenceVM.AbsenceDate);
                context.SaveChanges();
                AbsenceCancellationWindow mainWindow1 = (Application.Current.MainWindow as AbsenceCancellationWindow);
                absenceContext.AbsencesList = AllAbsences(mainWindow1.txtTeacherName.Text, mainWindow1.txtClass.Text);
                absenceContext.Message = "";
            }
        }

        public void BackMethod(object obj)
        {
            AbsenceCancellationWindow mainWindow = (Application.Current.MainWindow as AbsenceCancellationWindow);
            Application.Current.MainWindow = new ClassMasterWindow();
            Application.Current.MainWindow.Show();
            mainWindow.Close();
        }

        public void RefreshMethod(object obj)
        {
            ClassAbsencesVM absenceVM = obj as ClassAbsencesVM;

            if (absenceVM.TeacherName == null || absenceVM.TeacherName == "")
            {
                absenceContext.Message = "Trebuie sa precizati numele dumneavoastra si clasa la care sunteti diriginte!";
            }
            else
            {
                absenceContext.AbsencesList = AllAbsences(absenceVM.TeacherName, absenceVM.ClassName);
                absenceContext.Message = "";
            }
        }

        public ObservableCollection<ClassAbsencesVM> AllAbsences(string profesor, string clasa)
        {
            List<Absenta> absences = context.Absentas.ToList();
            ObservableCollection<ClassAbsencesVM> result = new ObservableCollection<ClassAbsencesVM>();
            List<Profesor> profi = context.Profesors.ToList();
            
            foreach (Profesor prof in profi)
            {
                if(profesor.Equals(prof.nume))
                {
                    if (prof.nume_clasa_dirig.Equals(clasa))
                    {
                        foreach (Absenta absenta in absences)
                        {
                            if (absenta.nume_clasa.Equals(clasa))
                                result.Add(new ClassAbsencesVM()
                                {
                                    AbsenceId = absenta.id_absenta,
                                    AbsenceDate = absenta.data_absenta,
                                    Nemotivabila = absenta.nemotivabila,
                                    Motivata = absenta.motivata,
                                    DisciplineId = absenta.id_materie,
                                    StudentCnp = absenta.cnp_elev,
                                    DisciplineName = absenta.nume_materie,
                                    TeacherName = absenta.nume_profesor,
                                    StudentName = absenta.nume_elev,
                                    ClassName = absenta.nume_clasa
                                });
                        }
                        return result;
                    }
                    else
                    {
                        MessageBox.Show("Nu sunteti dirigintele clasei introduse!");
                        return result;
                    }
                }
            }
            if (Application.Current.MainWindow.Title.Equals("Education - Edit Absences"))
            {
                MessageBox.Show("Profesorul nu se afla in baza de date!");
            }
            return result;
        }
    }
}

