﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tema_3.Helpers;
using Tema_3.ViewModels;
using Tema_3.Views;

namespace Tema_3.Models.Actions
{
    class CheckClassAbsenceActions : BaseVM
    {
        ScoalaEntities context = new ScoalaEntities();

        private CheckClassAbsenceVM absenceContext;
        public CheckClassAbsenceActions(CheckClassAbsenceVM absenceContext)
        {
            this.absenceContext = absenceContext;
        }

        public void SeeBySMethod(object obj)
        {
            CheckClassAbsenceVM absenceVM = obj as CheckClassAbsenceVM;
            if (absenceVM != null)
            {
                if (String.IsNullOrEmpty(absenceVM.StudentName))
                {
                    absenceContext.Message = "Numele tau trebuie precizat!";
                }
                else
                {
                    absenceContext.CheckAbsencesList = SomeAbsences(absenceVM.TeacherName, absenceVM.ClassName, absenceVM.StudentName);
                    absenceContext.Message = "";
                    ViewAbsences mainWindow2 = (Application.Current.MainWindow as ViewAbsences);
                    mainWindow2.absencesNumber.Content = "Numar absente: " + absenceContext.CheckAbsencesList.Count.ToString();
                }
            }
        }

        public void SeeAllMethod(object obj)
        {
            CheckClassAbsenceVM absenceVM = obj as CheckClassAbsenceVM;
            if (absenceVM != null)
            {
                if (String.IsNullOrEmpty(absenceVM.TeacherName))
                {
                    absenceContext.Message = "Numele dumneavoastra trebuie precizat!";
                }
                else
                {
                    absenceContext.CheckAbsencesList = AllAbsences(absenceVM.TeacherName, absenceVM.ClassName);
                    absenceContext.Message = "";
                    ViewAbsences mainWindow1 = (Application.Current.MainWindow as ViewAbsences);
                    mainWindow1.absencesNumber.Content = "Numar absente: " + absenceContext.CheckAbsencesList.Count.ToString();
                }
            }
        }

        public void BackMethod(object obj)
        {
            ViewAbsences mainWindow = (Application.Current.MainWindow as ViewAbsences);
            Application.Current.MainWindow = new ClassMasterWindow();
            Application.Current.MainWindow.Show();
            mainWindow.Close();
        }

        public ObservableCollection<CheckClassAbsenceVM> AllAbsences(string profesor, string clasa)
        {
            List<Absenta> absences = context.Absentas.ToList();
            ObservableCollection<CheckClassAbsenceVM> result = new ObservableCollection<CheckClassAbsenceVM>();
            List<Profesor> profi = context.Profesors.ToList();

            foreach (Profesor prof in profi)
            {
                if (profesor.Equals(prof.nume))
                {
                    if (prof.nume_clasa_dirig.Equals(clasa))
                    {
                        foreach (Absenta absenta in absences)
                        {
                            if (absenta.nume_clasa.Equals(clasa))
                                result.Add(new CheckClassAbsenceVM()
                                {
                                    AbsenceId = absenta.id_absenta,
                                    AbsenceDate = absenta.data_absenta,
                                    Nemotivabila = absenta.nemotivabila,
                                    Motivata = absenta.motivata,
                                    DisciplineId = absenta.id_materie,
                                    StudentCnp = absenta.cnp_elev,
                                    DisciplineName = absenta.nume_materie,
                                    TeacherName = absenta.nume_profesor,
                                    StudentName = absenta.nume_elev,
                                    ClassName = absenta.nume_clasa
                                });
                        }
                        return result;
                    }
                    else
                    {
                        MessageBox.Show("Nu sunteti dirigintele clasei introduse!");
                        return result;
                    }
                }
            }
            if (Application.Current.MainWindow.Title.Equals("Education - Edit Absences"))
            {
                MessageBox.Show("Profesorul nu se afla in baza de date!");
            }
            return result;
        }

        public ObservableCollection<CheckClassAbsenceVM> SomeAbsences(string profesor, string clasa, string elev)
        {
            List<Absenta> absences = context.Absentas.ToList();
            ObservableCollection<CheckClassAbsenceVM> result = new ObservableCollection<CheckClassAbsenceVM>();
            List<Profesor> profi = context.Profesors.ToList();

            foreach (Profesor prof in profi)
            {
                if (profesor.Equals(prof.nume))
                {
                    if (prof.nume_clasa_dirig.Equals(clasa))
                    {
                        List<Elev> elevi = context.Elevs.ToList();

                        foreach (Elev el in elevi)
                        { 
                            if (el.nume.Equals(elev))
                            {
                                if (el.nume_clasa.Equals(clasa))
                                {
                                    foreach (Absenta absenta in absences)
                                    {
                                        if (absenta.nume_clasa.Equals(clasa) && absenta.nume_elev.Equals(elev))
                                            result.Add(new CheckClassAbsenceVM()
                                            {
                                                AbsenceId = absenta.id_absenta,
                                                AbsenceDate = absenta.data_absenta,
                                                Nemotivabila = absenta.nemotivabila,
                                                Motivata = absenta.motivata,
                                                DisciplineId = absenta.id_materie,
                                                StudentCnp = absenta.cnp_elev,
                                                DisciplineName = absenta.nume_materie,
                                                TeacherName = absenta.nume_profesor,
                                                StudentName = absenta.nume_elev,
                                                ClassName = absenta.nume_clasa
                                            });
                                    }
                                    return result;
                                }
                                else
                                {
                                    MessageBox.Show("Elevul nu este inscris in clasa dumneavoastra!");
                                    return result;
                                }
                            }
                        }
                        if (Application.Current.MainWindow.Title.Equals("Education - See Absences"))
                        {
                            MessageBox.Show("Elevul nu se afla in baza de date!");
                        }
                        return result;
                    }
                    else
                    {
                        MessageBox.Show("Nu sunteti dirigintele clasei introduse!");
                        return result;
                    }
                }
            }
            if (Application.Current.MainWindow.Title.Equals("Education - See Absences"))
            {
                MessageBox.Show("Profesorul nu se afla in baza de date!");
            }
            return result;
        }
    }
}
