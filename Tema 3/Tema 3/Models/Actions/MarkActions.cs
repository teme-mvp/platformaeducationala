﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tema_3.Helpers;
using Tema_3.ViewModels;
using Tema_3.Views;

namespace Tema_3.Models.Actions
{
    class MarkActions : BaseVM
    {
        ScoalaEntities context = new ScoalaEntities();

        private MarkVM markContext;
        public MarkActions(MarkVM markContext)
        {
            this.markContext = markContext;
        }

        public void AddMethod(object obj)
        {
            MarkVM markVM = obj as MarkVM;
            if (markVM != null)
            {
                if (String.IsNullOrEmpty(markVM.TeacherName))
                {
                    markContext.Message = "Numele profesorului trebuie precizat";
                }
                else
                {
                    List<Profesor> profi = context.Profesors.ToList();
                    foreach (Profesor prof in profi)
                    {
                        if (prof.nume.Equals(markVM.TeacherName))
                        {
                            List<Profesor_Clasa> repartizari = context.Profesor_Clasa.ToList();
                            foreach (Profesor_Clasa rep in repartizari)
                            {
                                if (rep.nume_profesor.Equals(prof.nume))
                                {
                                    if (rep.nume_materie.Equals(markVM.DisciplineName))
                                    {
                                        context.InsertNota(context.Notas.OrderByDescending(p => p.id_nota).FirstOrDefault().id_nota + 1, markVM.ActualMark, markVM.MarkDate, context.Elevs.OrderByDescending(p => p.cnp_elev).FirstOrDefault().cnp_elev, markVM.DisciplineName, markVM.StudentName, markVM.TeacherName);
                                        context.SaveChanges();
                                        markContext.MarksList = AllMarks(markVM.TeacherName);
                                        markContext.Message = "";
                                        return;
                                    }
                                }
                            }
                            MessageBox.Show("Profesorul introdus nu preda materia introdusa!");
                            return;
                        }
                    }
                    MessageBox.Show("Profesorul introdus nu se afla in baza de date!");
                    return;
                }
            }
        }

        public void DeleteMethod(object obj)
        {
           MarkVM markVM = obj as MarkVM;

            if (markVM.TeacherName == null || markVM.TeacherName == "")
            {
                markContext.Message = "Selectati o nota!";
            }
            else
            {
                context.DeleteNota(markVM.TeacherName, markVM.MarkDate, markVM.DisciplineName, markVM.StudentName);
                context.SaveChanges();
                markContext.MarksList = AllMarks(markVM.TeacherName);
                markContext.Message = "";
            }
        }

        public void BackMethod(object obj)
        {
            EditNoteWindow mainWindow = (Application.Current.MainWindow as EditNoteWindow);
            Application.Current.MainWindow = new TeacherWindow();
            Application.Current.MainWindow.Show();
            mainWindow.Close();
        }

        public void RefreshMethod(object obj)
        {
            if (String.IsNullOrEmpty(obj.ToString()))
            {
                markContext.Message = "Trebuie sa precizati numele dumneavoastra!";
            }
            else
            {
                markContext.MarksList = AllMarks(obj.ToString());
                markContext.Message = "";
            }
        }

        public ObservableCollection<MarkVM> AllMarks(string profesor)
        {
            List<Nota> marks = context.Notas.ToList();
            ObservableCollection<MarkVM> result = new ObservableCollection<MarkVM>();
            foreach (Nota nota in marks)
            {
                if (nota.nume_profesor.Equals(profesor))
                    result.Add(new MarkVM()
                    {
                        MarkId = nota.id_nota,
                        ActualMark = nota.nota1,
                        MarkDate = nota.data_nota,
                        DisciplineId = nota.id_materie,
                        StudentCnp = nota.cnp_elev,
                        DisciplineName = nota.nume_materie,
                        StudentName = nota.nume_elev,
                        TeacherName = nota.nume_profesor
                    });
            }
            return result;
        }
    }
}

