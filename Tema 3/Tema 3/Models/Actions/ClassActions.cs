﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tema_3.Helpers;
using Tema_3.ViewModels;
using Tema_3.Views;

namespace Tema_3.Models.Actions
{
    class ClassActions: BaseVM
    {
        ScoalaEntities context = new ScoalaEntities();

        private ClassVM classContext;
        public ClassActions(ClassVM classContext)
        {
            this.classContext = classContext;
        }

        public void AddMethod(object obj)
        {
            ClassVM classVM = obj as ClassVM;
            if (classVM != null)
            {
                if (String.IsNullOrEmpty(classVM.ClassName))
                {
                    classContext.Message = "Numele clasei trebuie precizat";
                }
                else
                {
                   
                    context.Clasas.Add(new Clasa() { id_clasa = context.Clasas.OrderByDescending(p => p.id_clasa).FirstOrDefault().id_clasa + 1, nume_clasa = classVM.ClassName , specializare = classVM.Specializare});
                    context.SaveChanges();
                    classContext.ClassesList = AllClasses();
                    classContext.Message = "";
                }
            }
        }

        public void UpdateMethod(object obj)
        {
            ClassVM classVM = obj as ClassVM;
            
            if (String.IsNullOrEmpty(classVM.ClassName))
            {
                classContext.Message = "Numele clasei trebuie precizat";
            }
            else
            {
                context.UpdateClasa(classVM.ClassId, classVM.ClassName, classVM.Specializare);
                context.SaveChanges();
                classContext.ClassesList = AllClasses();
                classContext.Message = "";
            }
        }

        public void DeleteMethod(object obj)
        {
            ClassVM classVM = obj as ClassVM;

            if (classVM.ClassName == null || classVM.ClassName == "")
            {
                classContext.Message = "Selectati o clasa!";
            }
            else
            {

                context.DeleteClasa(classVM.ClassName);
                context.SaveChanges();
                classContext.ClassesList = AllClasses();
                classContext.Message = "";
            }
        }

        public void BackMethod(object obj)
        {
            EditClassWindow mainWindow = (Application.Current.MainWindow as EditClassWindow);
            Application.Current.MainWindow = new AdminWindow();
            Application.Current.MainWindow.Show();
            mainWindow.Close();
        }

        public ObservableCollection<ClassVM> AllClasses()
        {
            List<Clasa> classes = context.Clasas.ToList();
            ObservableCollection<ClassVM> result = new ObservableCollection<ClassVM>();
            foreach (Clasa clasa in classes)
            {
                result.Add(new ClassVM()
                {
                    ClassId = clasa.id_clasa,
                    ClassName = clasa.nume_clasa,
                    Specializare = clasa.specializare
                });
            }
            return result;
        }
    }
}
