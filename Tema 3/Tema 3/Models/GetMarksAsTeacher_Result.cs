//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tema_3.Models
{
    using System;
    
    public partial class GetMarksAsTeacher_Result
    {
        public string nume_elev { get; set; }
        public string nota { get; set; }
        public string data_nota { get; set; }
        public string nume_materie { get; set; }
        public string nume_profesor { get; set; }
    }
}
