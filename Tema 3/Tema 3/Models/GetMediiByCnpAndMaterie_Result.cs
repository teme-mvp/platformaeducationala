//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tema_3.Models
{
    using System;
    
    public partial class GetMediiByCnpAndMaterie_Result
    {
        public int id_medie { get; set; }
        public double media { get; set; }
        public int semestru { get; set; }
        public int id_materie { get; set; }
        public string cnp_elev { get; set; }
    }
}
