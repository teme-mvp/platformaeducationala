//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tema_3.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Elev
    {
        public Elev()
        {
            this.Absentas = new HashSet<Absenta>();
            this.Medies = new HashSet<Medie>();
            this.Notas = new HashSet<Nota>();
        }
    
        public string cnp_elev { get; set; }
        public string nume { get; set; }
        public string nr_tel { get; set; }
        public int id_clasa { get; set; }
        public int id_cont { get; set; }
        public string nume_clasa { get; set; }
    
        public virtual ICollection<Absenta> Absentas { get; set; }
        public virtual Clasa Clasa { get; set; }
        public virtual Cont Cont { get; set; }
        public virtual ICollection<Medie> Medies { get; set; }
        public virtual ICollection<Nota> Notas { get; set; }
    }
}
