﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Tema_3.ViewModels;

namespace Tema_3.Converters
{
    class AbsenceConvert : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null && values[2] != null && values[3] != null && values[4] != null && values[5] != null)
            {
                return new AbsenceVM()
                {
                    TeacherName = values[0].ToString(),
                    StudentName = values[1].ToString(),
                    AbsenceDate = values[2].ToString(),
                    Nemotivabila = values[3].ToString(),
                    DisciplineName = values[4].ToString(),
                    ClassName = values[5].ToString()
                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            AbsenceVM pers = value as AbsenceVM;
            object[] result = new object[5] { pers.TeacherName, pers.StudentName, pers.AbsenceDate, pers.Nemotivabila, pers.DisciplineName };
            return result;
        }
    }
}
