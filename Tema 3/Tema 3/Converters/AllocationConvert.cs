﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Tema_3.ViewModels;

namespace Tema_3.Converters
{
    class AllocationConvert : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null && values[2] != null && values[3] != null)
            {
                return new AllocationVM()
                {
                    TeacherCnp = values[0].ToString(),
                    TeacherName = values[1].ToString(),
                    ClassName = values[2].ToString(),
                    DisciplineName = values[3].ToString(),
                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            AllocationVM pers = value as AllocationVM;
            object[] result = new object[6] { pers.TeacherCnp, pers.ClassId, pers.DisciplineId, pers.TeacherName, pers.ClassName, pers.DisciplineName };
            return result;
        }
    }
}
