﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Tema_3.ViewModels;

namespace Tema_3.Converters
{
    class StudentConvert : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null && values[2] != null && values[3] != null && values[4] != null && values[5] != null)
            {
                return new StudentVM()
                {
                    StudentCnp = values[0].ToString(),
                    Name = values[1].ToString(),
                    PhoneNr = values[2].ToString(),
                    ClassName = values[3].ToString()
                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            StudentVM pers = value as StudentVM;
            object[] result = new object[6] { pers.StudentCnp, pers.Name, pers.PhoneNr, pers.ClassId, pers.AccountId, pers.ClassName };
            return result;
        }
    }
}
