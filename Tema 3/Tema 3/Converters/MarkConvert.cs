﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Tema_3.ViewModels;

namespace Tema_3.Converters
{
    class MarkConvert : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null && values[2] != null && values[3] != null && values[4] != null)
            {
                return new MarkVM()
                {
                    TeacherName = values[0].ToString(),
                    StudentName = values[1].ToString(),
                    MarkDate = values[2].ToString(),
                    ActualMark = values[3].ToString(),
                    DisciplineName = values[4].ToString()
                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            MarkVM pers = value as MarkVM;
            object[] result = new object[5] { pers.TeacherName, pers.StudentName, pers.MarkDate, pers.ActualMark, pers.DisciplineName};
            return result;
        }
    }
}

