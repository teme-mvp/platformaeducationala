﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Tema_3.ViewModels;

namespace Tema_3.Converters
{
    class CheckClassAbsenceConvert : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null)
            {
                return new CheckClassAbsenceVM()
                {
                    TeacherName = values[0].ToString(),
                    ClassName = values[1].ToString(),
                    StudentName = values[2].ToString()
                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            CheckClassAbsenceVM pers = value as CheckClassAbsenceVM;
            object[] result = new object[3] { pers.TeacherName, pers.ClassName, pers.StudentName };
            return result;
        }
    }
}