﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Tema_3.ViewModels;

namespace Tema_3.Converters
{
    class TeacherConvert : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null && values[2] != null && values[3] != null && values[4] != null && values[5] != null && values[6] != null)
            {
                return new TeacherVM()
                {
                    TeacherCnp = values[0].ToString(),
                    Name = values[1].ToString(),
                    PhoneNr = values[2].ToString(),
                    Role = values[3].ToString(),
                    ClassDirigName = values[4].ToString()
                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            TeacherVM pers = value as TeacherVM;
            object[] result = new object[7] { pers.TeacherCnp, pers.Name, pers.PhoneNr, pers.Role, pers.ClassDirId, pers.AccountId, pers.ClassDirigName };
            return result;
        }
    }
}