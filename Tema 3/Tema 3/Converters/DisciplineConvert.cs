﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Tema_3.ViewModels;

namespace Tema_3.Converters
{
    class DisciplineConvert : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null)
            {
                return new DisciplineVM()
                {
                    Name = values[0].ToString(),
                    NeedsT = values[1].ToString()
                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            DisciplineVM pers = value as DisciplineVM;
            object[] result = new object[2] { pers.Name, pers.NeedsT };
            return result;
        }
    }
}
