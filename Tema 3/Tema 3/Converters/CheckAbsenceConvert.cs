﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Tema_3.ViewModels;

namespace Tema_3.Converters
{
    class CheckAbsenceConvert : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null)
            {
                return new CheckAbsenceVM()
                {
                    StudentName = values[0].ToString(),
                    DisciplineName = values[1].ToString()
                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            CheckAbsenceVM pers = value as CheckAbsenceVM;
            object[] result = new object[2] { pers.StudentName, pers.DisciplineName };
            return result;
        }
    }
}