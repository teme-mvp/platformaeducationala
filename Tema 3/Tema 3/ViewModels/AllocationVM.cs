﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Tema_3.Commands;
using Tema_3.Helpers;
using Tema_3.Models.Actions;

namespace Tema_3.ViewModels
{
    class AllocationVM : BaseVM
    {
        AllocationActions pAct;

        public AllocationVM()
        {
            pAct = new AllocationActions(this);
        }

        #region Data Members

        private int id_profesor_clasa;
        private string cnp_profesor;
        private int id_clasa;
        private int id_materie;
        private string nume_profesor;
        private string nume_clasa;
        private string nume_materie;
        private string message;
        private ObservableCollection<AllocationVM> allocationsList;

        public int TeacherClassId
        {
            get
            {
                return id_profesor_clasa;
            }
            set
            {
                id_profesor_clasa = value;
                NotifyPropertyChanged("TeacherClassId");
            }
        }

        public string TeacherCnp
        {
            get
            {
                return cnp_profesor;
            }
            set
            {
                cnp_profesor = value;
                NotifyPropertyChanged("TeacherCnp");
            }
        }

        public int ClassId
        {
            get
            {
                return id_clasa;
            }
            set
            {
                id_clasa = value;
                NotifyPropertyChanged("ClassId");
            }
        }

        public int DisciplineId
        {
            get
            {
                return id_materie;
            }
            set
            {
                id_materie = value;
                NotifyPropertyChanged("DisciplineId");
            }
        }

        public string TeacherName
        {
            get
            {
                return nume_profesor;
            }
            set
            {
                nume_profesor = value;
                NotifyPropertyChanged("TeacherName");
            }
        }

        public string ClassName
        {
            get
            {
                return nume_clasa;
            }
            set
            {
                nume_clasa = value;
                NotifyPropertyChanged("ClassName");
            }
        }

        public string DisciplineName
        {
            get
            {
                return nume_materie;
            }
            set
            {
                nume_materie = value;
                NotifyPropertyChanged("DisciplineName");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }

        public ObservableCollection<AllocationVM> AllocationsList
        {
            get
            {
                allocationsList = pAct.AllAllocations();
                return allocationsList;
            }
            set
            {
                allocationsList = value;
                NotifyPropertyChanged("AllocationsList");
            }
        }

        #endregion

        #region Command Members

        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(pAct.AddMethod);
                }
                return addCommand;
            }
        }

        private ICommand updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new RelayCommand(pAct.UpdateMethod);
                }
                return updateCommand;
            }
        }

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new RelayCommand(pAct.DeleteMethod);
                }
                return deleteCommand;
            }
        }

        private ICommand backCommand;
        public ICommand BackCommand
        {
            get
            {
                if (backCommand == null)
                {
                    backCommand = new RelayCommand(pAct.BackMethod);
                }
                return backCommand;
            }
        }

        #endregion
    }
}