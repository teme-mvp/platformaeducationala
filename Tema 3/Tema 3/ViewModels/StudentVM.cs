﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Tema_3.Commands;
using Tema_3.Helpers;
using Tema_3.Models.Actions;

namespace Tema_3.ViewModels
{
    class StudentVM : BaseVM
    {
        StudentActions pAct;

        public StudentVM()
        {
            pAct = new StudentActions(this);
        }

        #region Data Members

        private string cnp_elev;
        private string nume;
        private string nr_tel;
        private int id_clasa;
        private int id_cont;
        private string nume_clasa;
        private string message;
        private ObservableCollection<StudentVM> studentsList;

        public string StudentCnp
        {
            get
            {
                return cnp_elev;
            }
            set
            {
                cnp_elev = value;
                NotifyPropertyChanged("StudentCnp");
            }
        }

        public string Name
        {
            get
            {
                return nume;
            }
            set
            {
                nume = value;
                NotifyPropertyChanged("Name");
            }
        }

        public string PhoneNr
        {
            get
            {
                return nr_tel;
            }
            set
            {
                nr_tel = value;
                NotifyPropertyChanged("PhoneNr");
            }
        }

        public int ClassId
        {
            get
            {
                return id_clasa;
            }
            set
            {
                id_clasa = value;
                NotifyPropertyChanged("ClassId");
            }
        }

        public int AccountId
        {
            get
            {
                return id_cont;
            }
            set
            {
                id_cont = value;
                NotifyPropertyChanged("AccountId");
            }
        }

        public string ClassName
        {
            get
            {
                return nume_clasa;
            }
            set
            {
                nume_clasa = value;
                NotifyPropertyChanged("ClassName");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }

        public ObservableCollection<StudentVM> StudentsList
        {
            get
            {
                studentsList = pAct.AllStudents();
                return studentsList;
            }
            set
            {
                studentsList = value;
                NotifyPropertyChanged("StudentsList");
            }
        }

        #endregion

        #region Command Members

        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(pAct.AddMethod);
                }
                return addCommand;
            }
        }

        private ICommand updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new RelayCommand(pAct.UpdateMethod);
                }
                return updateCommand;
            }
        }

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new RelayCommand(pAct.DeleteMethod);
                }
                return deleteCommand;
            }
        }

        private ICommand backCommand;
        public ICommand BackCommand
        {
            get
            {
                if (backCommand == null)
                {
                    backCommand = new RelayCommand(pAct.BackMethod);
                }
                return backCommand;
            }
        }

        #endregion
    }
}
