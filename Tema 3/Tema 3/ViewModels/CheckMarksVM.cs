﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Tema_3.Commands;
using Tema_3.Helpers;
using Tema_3.Models.Actions;
using Tema_3.Views;

namespace Tema_3.ViewModels
{
    class CheckMarksVM : BaseVM
    {
        CheckMarkActions pAct;

        public CheckMarksVM()
        {
            pAct = new CheckMarkActions(this);
        }

        #region Data Members

        private int id_nota;
        private string nota;
        private string data_nota;
        private int id_materie;
        private string cnp_elev;
        private string nume_materie;
        private string nume_elev;
        private string nume_profesor;
        private string message;
        private ObservableCollection<CheckMarksVM> checkMarksList;

        public int MarkId
        {
            get
            {
                return id_nota;
            }
            set
            {
                id_nota = value;
                NotifyPropertyChanged("MarkId");
            }
        }

        public string ActualMark
        {
            get
            {
                return nota;
            }
            set
            {
                nota = value;
                NotifyPropertyChanged("ActualMark");
            }
        }

        public string MarkDate
        {
            get
            {
                return data_nota;
            }
            set
            {
                data_nota = value;
                NotifyPropertyChanged("MarkDate");
            }
        }

        public int DisciplineId
        {
            get
            {
                return id_materie;
            }
            set
            {
                id_materie = value;
                NotifyPropertyChanged("DisciplineId");
            }
        }

        public string StudentCnp
        {
            get
            {
                return cnp_elev;
            }
            set
            {
                cnp_elev = value;
                NotifyPropertyChanged("StudentCnp");
            }
        }

        public string DisciplineName
        {
            get
            {
                return nume_materie;
            }
            set
            {
                nume_materie = value;
                NotifyPropertyChanged("DisciplineName");
            }
        }

        public string StudentName
        {
            get
            {
                return nume_elev;
            }
            set
            {
                nume_elev = value;
                NotifyPropertyChanged("StudentName");
            }
        }

        public string TeacherName
        {
            get
            {
                return nume_profesor;
            }
            set
            {
                nume_profesor = value;
                NotifyPropertyChanged("TeacherName");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }

        public ObservableCollection<CheckMarksVM> CheckMarksList
        {
            get
            {
                if (Application.Current.MainWindow.Title.Equals("Education - See Marks"))
                {
                    CheckMarksWindow thisWindow = (Application.Current.MainWindow as CheckMarksWindow);
                    if(!String.IsNullOrEmpty(thisWindow.txtDiscipline.Text))
                        checkMarksList = pAct.SomeMarks(thisWindow.txtStudentName.Text, thisWindow.txtDiscipline.Text);
                    else
                        checkMarksList = pAct.AllMarks(thisWindow.txtStudentName.Text);
                }
                else
                {
                    checkMarksList = pAct.SomeMarks(" ", " ");
                }
                return checkMarksList;
            }
            set
            {
                checkMarksList = value;
                NotifyPropertyChanged("CheckMarksList");
            }
        }

        #endregion

        #region Command Members

        private ICommand seeByDiscipline;
        public ICommand SeeByDiscipline
        {
            get
            {
                if (seeByDiscipline == null)
                {
                    seeByDiscipline = new RelayCommand(pAct.SeeByDMethod);
                }
                return seeByDiscipline;
            }
        }

        private ICommand seeAll;
        public ICommand SeeAll
        {
            get
            {
                if (seeAll == null)
                {
                    seeAll = new RelayCommand(pAct.SeeAllMethod);
                }
                return seeAll;
            }
        }

        private ICommand backCommand;
        public ICommand BackCommand
        {
            get
            {
                if (backCommand == null)
                {
                    backCommand = new RelayCommand(pAct.BackMethod);
                }
                return backCommand;
            }
        }
        #endregion
    }
}