﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Tema_3.Commands;
using Tema_3.Helpers;
using Tema_3.Models.Actions;
using Tema_3.Views;

namespace Tema_3.ViewModels
{
    class CheckAbsenceVM : BaseVM
    {
        CheckAbsenceActions pAct;

        public CheckAbsenceVM()
        {
            pAct = new CheckAbsenceActions(this);
        }

        #region Data Members

        private int id_absenta;
        private string data_absenta;
        private string nemotivabila;
        private string motivata;
        private int id_materie;
        private string cnp_elev;
        private string nume_materie;
        private string nume_profesor;
        private string nume_elev;
        private string nume_clasa;
        private string message;
        private ObservableCollection<CheckAbsenceVM> checkAbsencesList;

        public int AbsenceId
        {
            get
            {
                return id_absenta;
            }
            set
            {
                id_absenta = value;
                NotifyPropertyChanged("AbsenceId");
            }
        }

        public string AbsenceDate
        {
            get
            {
                return data_absenta;
            }
            set
            {
                data_absenta = value;
                NotifyPropertyChanged("AbsenceDate");
            }
        }

        public string Nemotivabila
        {
            get
            {
                return nemotivabila;
            }
            set
            {
                nemotivabila = value;
                NotifyPropertyChanged("Nemotivabila");
            }
        }

        public string Motivata
        {
            get
            {
                return motivata;
            }
            set
            {
                motivata = value;
                NotifyPropertyChanged("Motivata");
            }
        }

        public int DisciplineId
        {
            get
            {
                return id_materie;
            }
            set
            {
                id_materie = value;
                NotifyPropertyChanged("DisciplineId");
            }
        }

        public string StudentCnp
        {
            get
            {
                return cnp_elev;
            }
            set
            {
                cnp_elev = value;
                NotifyPropertyChanged("StudentCnp");
            }
        }

        public string DisciplineName
        {
            get
            {
                return nume_materie;
            }
            set
            {
                nume_materie = value;
                NotifyPropertyChanged("DisciplineName");
            }
        }

        public string TeacherName
        {
            get
            {
                return nume_profesor;
            }
            set
            {
                nume_profesor = value;
                NotifyPropertyChanged("TeacherName");
            }
        }

        public string StudentName
        {
            get
            {
                return nume_elev;
            }
            set
            {
                nume_elev = value;
                NotifyPropertyChanged("StudentName");
            }
        }

        public string ClassName
        {
            get
            {
                return nume_clasa;
            }
            set
            {
                nume_clasa = value;
                NotifyPropertyChanged("ClassName");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }
        public ObservableCollection<CheckAbsenceVM> CheckAbsencesList
        {
            get
            {
                if (Application.Current.MainWindow.Title.Equals("Education - See Absences"))
                {
                    CheckAbsencesWindow thisWindow = (Application.Current.MainWindow as CheckAbsencesWindow);
                    if (!String.IsNullOrEmpty(thisWindow.txtDiscipline.Text))
                        checkAbsencesList = pAct.SomeAbsences(thisWindow.txtStudentName.Text, thisWindow.txtDiscipline.Text);
                    else
                        checkAbsencesList = pAct.AllAbsences(thisWindow.txtStudentName.Text);
                }
                else
                {
                    checkAbsencesList = pAct.SomeAbsences(" ", " ");
                }
                return checkAbsencesList;
            }
            set
            {
                checkAbsencesList = value;
                NotifyPropertyChanged("CheckAbsencesList");
            }
        }

#endregion

        #region Command Members

        private ICommand seeByDiscipline;
        public ICommand SeeByDiscipline
        {
            get
            {
                if (seeByDiscipline == null)
                {
                    seeByDiscipline = new RelayCommand(pAct.SeeByDMethod);
                }
                return seeByDiscipline;
            }
        }

        private ICommand seeAll;
        public ICommand SeeAll
        {
            get
            {
                if (seeAll == null)
                {
                    seeAll = new RelayCommand(pAct.SeeAllMethod);
                }
                return seeAll;
            }
        }

        private ICommand backCommand;
        public ICommand BackCommand
        {
            get
            {
                if (backCommand == null)
                {
                    backCommand = new RelayCommand(pAct.BackMethod);
                }
                return backCommand;
            }
        }
        #endregion
    }
}
