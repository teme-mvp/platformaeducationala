﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Tema_3.Commands;
using Tema_3.Helpers;
using Tema_3.Models.Actions;

namespace Tema_3.ViewModels
{
    class DisciplineVM : BaseVM
    {
        DisciplineActions pAct;

        public DisciplineVM()
        {
            pAct = new DisciplineActions(this);
        }

        #region Data Members

        private int id_materie;
        private string denumire;
        private string needs_teza;
        private string message;
        private ObservableCollection<DisciplineVM> disciplinesList;

        public int DisciplineId
        {
            get
            {
                return id_materie;
            }
            set
            {
                id_materie = value;
                NotifyPropertyChanged("DisciplineId");
            }
        }

        public string Name
        {
            get
            {
                return denumire;
            }
            set
            {
                denumire = value;
                NotifyPropertyChanged("Name");
            }
        }

        public string NeedsT
        {
            get
            {
                return needs_teza;
            }
            set
            {
                needs_teza = value;
                NotifyPropertyChanged("NeedsT");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }

        public ObservableCollection<DisciplineVM> DisciplinesList
        {
            get
            {
                disciplinesList = pAct.AllDisciplines();
                return disciplinesList;
            }
            set
            {
                disciplinesList = value;
                NotifyPropertyChanged("DisciplinesList");
            }
        }

        #endregion

        #region Command Members

        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(pAct.AddMethod);
                }
                return addCommand;
            }
        }

        private ICommand updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new RelayCommand(pAct.UpdateMethod);
                }
                return updateCommand;
            }
        }

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new RelayCommand(pAct.DeleteMethod);
                }
                return deleteCommand;
            }
        }

        private ICommand backCommand;
        public ICommand BackCommand
        {
            get
            {
                if (backCommand == null)
                {
                    backCommand = new RelayCommand(pAct.BackMethod);
                }
                return backCommand;
            }
        }

        #endregion
    }
}
