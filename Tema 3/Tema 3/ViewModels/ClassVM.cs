﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Tema_3.Commands;
using Tema_3.Helpers;
using Tema_3.Models.Actions;

namespace Tema_3.ViewModels
{
    class ClassVM : BaseVM
    {
        ClassActions pAct;

        public ClassVM()
        {
            pAct = new ClassActions(this);
        }

        #region Data Members

        private int id_clasa;
        private string nume_clasa;
        private string specializare;
        private string message;
        private ObservableCollection<ClassVM> classesList;

        public int ClassId
        {
            get
            {
                return id_clasa;
            }
            set
            {
                id_clasa = value;
                NotifyPropertyChanged("ClassId");
            }
        }

        public string ClassName
        {
            get
            {
                return nume_clasa;
            }
            set
            {
                nume_clasa = value;
                NotifyPropertyChanged("ClassName");
            }
        }

        public string Specializare
        {
            get
            {
                return specializare;
            }
            set
            {
                specializare = value;
                NotifyPropertyChanged("Specializare");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }

        public ObservableCollection<ClassVM> ClassesList
        {
            get
            {
                classesList = pAct.AllClasses();
                return classesList;
            }
            set
            {
                classesList = value;
                NotifyPropertyChanged("ClassesList");
            }
        }

        #endregion

        #region Command Members

        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(pAct.AddMethod);
                }
                return addCommand;
            }
        }

        private ICommand updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new RelayCommand(pAct.UpdateMethod);
                }
                return updateCommand;
            }
        }

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new RelayCommand(pAct.DeleteMethod);
                }
                return deleteCommand;
            }
        }

        private ICommand backCommand;
        public ICommand BackCommand
        {
            get
            {
                if (backCommand == null)
                {
                    backCommand = new RelayCommand(pAct.BackMethod);
                }
                return backCommand;
            }
        }

        #endregion
    }
}
