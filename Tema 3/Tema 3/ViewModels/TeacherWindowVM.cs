﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Tema_3.Commands;
using Tema_3.Views;

namespace Tema_3.ViewModels
{
    class TeacherWindowVM
    {
        private ICommand openWindowCommand;
        public ICommand OpenWindowCommand
        {
            get
            {
                if (openWindowCommand == null)
                {
                    openWindowCommand = new RelayCommand(OpenWindow);
                }
                return openWindowCommand;
            }
        }

        public void OpenWindow(object obj)
        {
            string nr = obj as string;
            switch (nr)
            {
                case "1":
                    TeacherWindow mainWindow1 = (Application.Current.MainWindow as TeacherWindow);
                    Application.Current.MainWindow = new EditNoteWindow();
                    Application.Current.MainWindow.Show();
                    mainWindow1.Close();
                    break;
                
                case "2":
                    TeacherWindow mainWindow2 = (Application.Current.MainWindow as TeacherWindow);
                    Application.Current.MainWindow = new EditAbsenceWindow();
                    Application.Current.MainWindow.Show();
                    mainWindow2.Close();
                    break;

                case "3":

                    break;

                case "4":
                    TeacherWindow mainWindow = (Application.Current.MainWindow as TeacherWindow);
                    Application.Current.MainWindow = new MainWindow();
                    Application.Current.MainWindow.Show();
                    mainWindow.Close();
                    break;
            }
        }
    }
}
