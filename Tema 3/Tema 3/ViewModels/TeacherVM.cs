﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Tema_3.Commands;
using Tema_3.Helpers;
using Tema_3.Models.Actions;

namespace Tema_3.ViewModels
{
    class TeacherVM : BaseVM
    {
        TeacherActions pAct;

        public TeacherVM()
        {
            pAct = new TeacherActions(this);
        }

        #region Data Members

        private string cnp_profesor;
        private string nume;
        private string nr_tel;
        private string rol;
        private int id_clasa_dirig;
        private int id_cont;
        private string nume_clasa_dirig;
        private string message;
        private ObservableCollection<TeacherVM> teachersList;

        public string TeacherCnp
        {
            get
            {
                return cnp_profesor;
            }
            set
            {
                cnp_profesor = value;
                NotifyPropertyChanged("TeacherCnp");
            }
        }

        public string Name
        {
            get
            {
                return nume;
            }
            set
            {
                nume = value;
                NotifyPropertyChanged("Name");
            }
        }

        public string PhoneNr
        {
            get
            {
                return nr_tel;
            }
            set
            {
                nr_tel = value;
                NotifyPropertyChanged("PhoneNr");
            }
        }

        public string Role
        {
            get
            {
                return rol;
            }
            set
            {
                rol = value;
                NotifyPropertyChanged("Role");
            }
        }

        public int ClassDirId
        {
            get
            {
                return id_clasa_dirig;
            }
            set
            {
                id_clasa_dirig = value;
                NotifyPropertyChanged("ClassDirId");
            }
        }

        public int AccountId
        {
            get
            {
                return id_cont;
            }
            set
            {
                id_cont = value;
                NotifyPropertyChanged("AccountId");
            }
        }

        public string ClassDirigName
        {
            get
            {
                return nume_clasa_dirig;
            }
            set
            {
                nume_clasa_dirig = value;
                NotifyPropertyChanged("ClassDirigName");
            }
        }
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }

        public ObservableCollection<TeacherVM> TeachersList
        {
            get
            {
                teachersList = pAct.AllTeachers();
                return teachersList;
            }
            set
            {
                teachersList = value;
                NotifyPropertyChanged("TeachersList");
            }
        }

        #endregion

        #region Command Members

        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(pAct.AddMethod);
                }
                return addCommand;
            }
        }

        private ICommand updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new RelayCommand(pAct.UpdateMethod);
                }
                return updateCommand;
            }
        }

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new RelayCommand(pAct.DeleteMethod);
                }
                return deleteCommand;
            }
        }

        private ICommand backCommand;
        public ICommand BackCommand
        {
            get
            {
                if (backCommand == null)
                {
                    backCommand = new RelayCommand(pAct.BackMethod);
                }
                return backCommand;
            }
        }

        #endregion
    }
}
