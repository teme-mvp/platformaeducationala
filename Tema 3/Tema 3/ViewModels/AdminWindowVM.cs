﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Tema_3.Commands;
using Tema_3.Views;

namespace Tema_3.ViewModels
{
    class AdminWindowVM
    {
        private ICommand openWindowCommand;
        public ICommand OpenWindowCommand
        {
            get
            {
                if (openWindowCommand == null)
                {
                    openWindowCommand = new RelayCommand(OpenWindow);
                }
                return openWindowCommand;
            }
        }

        public void OpenWindow(object obj)
        {
            string nr = obj as string;
            switch (nr)
            {
                case "1":
                    AdminWindow mainWindow1 = (Application.Current.MainWindow as AdminWindow);
                    Application.Current.MainWindow = new EditStudentWindow();
                    Application.Current.MainWindow.Show();
                    mainWindow1.Close();
                    break;

                case "2":
                    AdminWindow mainWindow2 = (Application.Current.MainWindow as AdminWindow);
                    Application.Current.MainWindow = new EditTeacherWindow();
                    Application.Current.MainWindow.Show();
                    mainWindow2.Close();
                    break;

                case "3":
                    AdminWindow mainWindow3 = (Application.Current.MainWindow as AdminWindow);
                    Application.Current.MainWindow = new EditDisciplineWindow();
                    Application.Current.MainWindow.Show();
                    mainWindow3.Close();
                    break;

                case "4":
                    AdminWindow mainWindow4 = (Application.Current.MainWindow as AdminWindow);
                    Application.Current.MainWindow = new EditClassWindow();
                    Application.Current.MainWindow.Show();
                    mainWindow4.Close();
                    break;

                case "5":
                    AdminWindow mainWindow = (Application.Current.MainWindow as AdminWindow);
                    Application.Current.MainWindow = new MainWindow();
                    Application.Current.MainWindow.Show();
                    mainWindow.Close();
                    break;

                case "6":
                    AdminWindow mainWindow5 = (Application.Current.MainWindow as AdminWindow);
                    Application.Current.MainWindow = new EditAllocationWindow();
                    Application.Current.MainWindow.Show();
                    mainWindow5.Close();
                    break;
            }
        }
    }
}
